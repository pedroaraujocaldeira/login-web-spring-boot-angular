import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { Observable } from 'rxjs';
import { take, catchError } from 'rxjs/operators';
import { User } from 'src/app/core/models/user.model';
import { ProfilesService } from 'src/app/core/services/profiles.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileAuthResolverService implements Resolve<User> {
  constructor(
    private profilesService: ProfilesService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {

    console.log('sdf');
    return this.profilesService.get(route.params.username)
      .pipe(catchError((err) => this.router.navigateByUrl('/login')));

  }
}

