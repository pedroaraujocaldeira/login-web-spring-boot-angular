import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  displayedColumns = ['date_posted', 'title', 'category', 'delete'];
  dataSource = [
    {position: 0, title: 'Post One', category: 'Web Development', date_posted: new Date(), body: 'Body 1'},
    {position: 1, title: 'Post Two', category: 'Android Development', date_posted: new Date(), body: 'Body 2'},
    {position: 2, title: 'Post Three', category: 'IOS Development', date_posted: new Date(), body: 'Body 3'},
    {position: 3, title: 'Post Four', category: 'Android Development', date_posted: new Date(), body: 'Body 4'},
    {position: 4, title: 'Post Five', category: 'IOS Development', date_posted: new Date(), body: 'Body 5'},
    {position: 5, title: 'Post Six', category: 'Web Development', date_posted: new Date(), body: 'Body 6'},
  ];

  constructor(    private userService: UserService,
                  private router: Router,

    ) {
  }

    ngOnInit(): void {
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['/login']);
    }


  deletePost(id) {
    // if (this.auth.isAuthenticated()) {
    //   this.dataService.deletePost(id);
    //   this.dataSource = new PostDataSource(this.dataService);
    // } else {
    //   alert('Login in Before');
    // }
  }

  openDialog(): void {
    // let dialogRef = this.dialog.open(PostDialogComponent, {
    //   width: '600px',
    //   data: 'Add Post'
    // });
    // dialogRef.componentInstance.event.subscribe((result) => {
    //   this.dataService.addPost(result.data);
    //   this.dataSource = new PostDataSource(this.dataService);
    // });
  }

}



