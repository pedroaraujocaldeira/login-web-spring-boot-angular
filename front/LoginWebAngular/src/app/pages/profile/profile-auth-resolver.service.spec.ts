/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProfileAuthResolverService } from './profile-auth-resolver.service';

describe('Service: ProfileAuthResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileAuthResolverService]
    });
  });

  it('should ...', inject([ProfileAuthResolverService], (service: ProfileAuthResolverService) => {
    expect(service).toBeTruthy();
  }));
});
