import { ProfilesService } from 'src/app/core/services/profiles.service';
import { NgModule } from '@angular/core';
import { ProfileComponent } from './profile.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProfileRoutes } from './profile.routing';

@NgModule({
  imports: [
    SharedModule,
    ProfileRoutes,
  ],
  declarations: [ProfileComponent],
  providers: [
    ProfilesService
  ]
})
export class ProfileModule { }
