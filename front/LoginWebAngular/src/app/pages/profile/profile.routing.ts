import { AuthGuardService } from './../../core/guards/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ProfileAuthResolverService } from './profile-auth-resolver.service';

const routes: Routes = [
  {
    path: ':username',
    component: ProfileComponent,
    // resolve: {
    //   profile: ProfileAuthResolverService
    // },
    canActivate: [AuthGuardService]

  },
];

export const ProfileRoutes = RouterModule.forChild(routes);
