import { NoAuthGuardService } from './../guards/no-auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationComponent } from './authentication.component';

const routes: Routes = [
  {
    path: 'login',
    component: AuthenticationComponent,
    canActivate: [NoAuthGuardService]
  },
  // {
  //   path: 'register',
  //   component: AuthenticationComponent,
  //   canActivate: [NoAuthGuardService]
  // }
];

export const AuthenticationRoutes = RouterModule.forChild(routes);
