import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { Errors } from '../models/errors.model';
import { ErrorStateMatcher, MatDialog } from '@angular/material';
import { ProgressService } from '../services/progress.service';
import { ProgressComponent } from 'src/app/shared/progress/progress.component';
import { MessagemService } from '../services/messagem.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid);
  }
}

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {

  createForm: FormGroup;
  loginForm: FormGroup;
  matcher;

  constructor(private formBuilder: FormBuilder, private authService: UserService, private progress: ProgressService,
              public router: Router, public dialog: MatDialog, private messageService: MessagemService
              ) { }

  ngOnInit() {
    this.createForm = new FormGroup({
      password: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.pattern('[a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]{3,150}')]),
      mail: new FormControl('', [Validators.email, Validators.required]),
      phone: new FormControl('', [Validators.required,
      Validators.pattern('\(([0-9]{2}|0{1}((x|[0-9]){2}[0-9]{2}))\)\s*[0-9]{4,5}[- ]*[0-9]{4}')]),

    });
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
    this.matcher = new MyErrorStateMatcher();

  }

  login() {

    this.progress.ativarCarregamento(true);
    this.authService.login(this.loginForm.getRawValue()).subscribe(
      data => {
          this.router.navigate([`profile/username`]);
      },
      error => {
        this.progress.ativarCarregamento(false);
        this.messageService.exibirMensagem('Tente Novamente!');
      },
      () => {
        this.progress.ativarCarregamento(false);
      }
    );
  }
  create() {
    this.progress.ativarCarregamento(true);

    this.authService.create(this.createForm.getRawValue()).subscribe(
      data => {
        this.messageService.exibirMensagem(data.body);
      },
      error => {
        this.progress.ativarCarregamento(false);
        this.messageService.exibirMensagem('Tente Novamente!');
      },
      () => {
        this.progress.ativarCarregamento(false);
      }
    );
  }



  // authType = '';
  // title = '';
  // errors: Errors = {errors: {}};
  // isSubmitting = false;
  // authForm: FormGroup;

  // constructor(
  //   private route: ActivatedRoute,
  //   private router: Router,
  //   private userService: UserService,
  //   private fb: FormBuilder
  // ) {
  //   // use FormBuilder to create a form group
  //   this.authForm = this.fb.group({
  //     username: ['', Validators.required],
  //     password: ['', Validators.required]
  //   });
  // }

  // ngOnInit() {

  //   if (this.router.url === '/login') {
  //     this.authType = 'login';
  //   }
  //   if (this.router.url === '/register') {
  //     this.authForm.addControl('username', new FormControl());
  //     this.authType = 'register';
  //   }
  //   this.title = (this.authType === 'login') ? 'Acessar' : 'Cadastrar';

  // }

  // submitForm() {
  //   this.isSubmitting = true;
  //   this.errors = {errors: {}};

  //   const credentials = this.authForm.value;
  //   this.userService
  //   .attemptAuth(this.authType, credentials)
  //   .subscribe(
  //     data => this.router.navigateByUrl(`/profile/alo`),
  //     err => {
  //       this.errors = err;
  //       this.isSubmitting = false;
  //     }
  //   );
  // }
}
