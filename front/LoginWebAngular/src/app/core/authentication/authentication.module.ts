import { NoAuthGuardService } from './../guards/no-auth-guard.service';
import { NgModule } from '@angular/core';
import { AuthenticationComponent } from './authentication.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AuthenticationRoutes } from './authentication.routing';

@NgModule({
  imports: [
    SharedModule,
    AuthenticationRoutes
  ],
  declarations: [AuthenticationComponent],
  providers: [
    NoAuthGuardService
  ]
})
export class AuthenticationModule { }
