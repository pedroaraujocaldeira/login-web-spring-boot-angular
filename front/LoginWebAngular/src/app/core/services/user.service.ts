import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient,
    private jwtService: JwtService
  ) { }
  private currentUserSubject = new BehaviorSubject<User>({} as User);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();


  populate() {
    if (this.jwtService.getToken()) {
      console.log('logado');
      this.isAuthenticatedSubject.next(true);

      // this.apiService.get('/user')
      //   .subscribe(
      //     data => this.setAuth(data.user),
      //     err => this.purgeAuth()
      //   );
    } else {
      this.purgeAuth();
    }
  }

  setAuth(token: string) {
    this.jwtService.saveToken(token);
    this.isAuthenticatedSubject.next(true);
  }

  purgeAuth() {
    this.jwtService.destroyToken();
    this.isAuthenticatedSubject.next(false);
  }

  logout() {
    this.purgeAuth();
  }

  // attemptAuth(type, credentials): Observable<User> {
  //   const route = (type === 'login') ? '/authenticate' : '';
  //   return this.apiService.post('/users' + route,  credentials )
  //     .pipe(map(
  //       data => {
  //         this.setAuth({email: credentials.username, token: data.jwt, username: credentials.username});
  //         return {email: credentials.username, token: data.jwt, username: credentials.username};
  //       }
  //     ));
  // }


  login(credentials: User) {

    return this.apiService.post('/users/authenticate',  credentials )
    .pipe(map(
      data => {
        this.setAuth(data.jwt);
        return true;
      },
      error => {
        return false;
      }
    ));

  }

  create(credentials: User) {
    return this.apiService.post('/users',  credentials )
    .pipe(map(
      data => {
        return data;
      }
    ));
  }

  getCurrentUser(): User {
    return this.currentUserSubject.value;
  }

  // Update the user on the server (email, pass, etc)
  update(user): Observable<User> {
    return this.apiService
      .put('/user', { user })
      .pipe(map(data => {
        // Update the currentUser observable
        this.currentUserSubject.next(data.user);
        return data.user;
      }));
  }
}
