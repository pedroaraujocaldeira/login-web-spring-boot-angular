import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {

  public loading: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ativarCarregamento(ativar: boolean) {
    this.loading.emit(ativar);
  }

}
