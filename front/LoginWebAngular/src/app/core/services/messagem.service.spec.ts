/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MessagemService } from './messagem.service';

describe('Service: Messagem', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessagemService]
    });
  });

  it('should ...', inject([MessagemService], (service: MessagemService) => {
    expect(service).toBeTruthy();
  }));
});
