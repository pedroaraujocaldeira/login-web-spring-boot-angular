import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagemService {

  public loading: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  exibirMensagem(message: string) {
    this.loading.emit(message);
  }

}
