import { User } from './../models/user.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {
  constructor (
    private apiService: ApiService
  ) {}

  get(username: string): Observable<User> {
    return this.apiService.get('/profiles/' + username)
      .pipe(map((data: {profile: User}) => data.profile));
  }

  follow(username: string): Observable<User> {
    return this.apiService.post('/profiles/' + username + '/follow');
  }

  unfollow(username: string): Observable<User> {
    return this.apiService.delete('/profiles/' + username + '/follow');
  }

}
