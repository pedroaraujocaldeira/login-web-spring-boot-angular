export interface User {
  mail: string;
  username: string;
  phone: string;
  password: string;
  token: string;

}
