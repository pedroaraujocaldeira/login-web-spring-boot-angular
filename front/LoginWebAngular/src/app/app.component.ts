import { MessagemService } from './core/services/messagem.service';
import { MessagemComponent } from './shared/messagem/messagem.component';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from './core/services/user.service';
import { Subscription } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ProgressService } from './core/services/progress.service';
import { ProgressComponent } from './shared/progress/progress.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'Zero To Project';
  private subscribeLogin: Subscription = new Subscription();
  private subscribeMessage: Subscription = new Subscription();
  public dialogMensagem: MatDialogRef<MessagemComponent>;
  public dialogLoading: MatDialogRef<ProgressComponent>;

  constructor(
    private userService: UserService,
    public dialog: MatDialog,
    public progress: ProgressService,
    public message: MessagemService
  ) { }

  ngOnInit() {
    this.userService.populate();
    this.subscribeLogin = this.progress.loading.subscribe((ativar => {
      if (ativar) {
        this.dialogLoading =  this.openDialogLoading();
      } else {
        this.dialogLoading.close();
      }
    }));

    this.subscribeMessage = this.message.loading.subscribe((message => {
      this.dialogMensagem = this.openDialogMessagem(message);

    }));

  }

  openDialogLoading(): MatDialogRef<ProgressComponent>{
   return this.dialog.open(ProgressComponent, {
      panelClass: 'custom-modalbox'
    });
  }
  openDialogMessagem(message): MatDialogRef<MessagemComponent> {
    return this.dialog.open(MessagemComponent, {
      data: {
        message
      }
    });
  }

  ngOnDestroy(): void {
    this.subscribeMessage.unsubscribe();
    this.subscribeLogin.unsubscribe();

  }

}
