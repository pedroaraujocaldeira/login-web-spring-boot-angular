import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-messagem',
  templateUrl: './messagem.component.html',
  styleUrls: ['./messagem.component.css']
})
export class MessagemComponent implements OnInit {


  mensagem = '';
  constructor(private dialogRef: MatDialogRef<MessagemComponent>,
              @Inject(MAT_DIALOG_DATA) data) {

    this.mensagem = data.message;

  }

  ngOnInit(): void {
  }
}
