import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedComponent } from './shared.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ListErrorsComponent } from './list-errors/list-errors.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { ProgressComponent } from './progress/progress.component';
import { MessagemComponent } from './messagem/messagem.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    MaterialModule

  ],
  declarations: [SharedComponent,
    ListErrorsComponent, ProgressComponent, MessagemComponent],
  exports: [

    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    ListErrorsComponent,
    MaterialModule

  ],
  entryComponents: [ProgressComponent, MessagemComponent]

})
export class SharedModule { }
