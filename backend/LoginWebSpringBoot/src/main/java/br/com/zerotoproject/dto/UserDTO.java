package br.com.zerotoproject.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class UserDTO {

	Integer id;

	@NotNull
	@NotBlank
	String name;

	@NotNull
	@NotBlank
	@Email
	String mail;

	@NotNull
	@NotBlank
	String password;

	@NotNull
	@NotBlank
	String phone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserDTO() {
	}

	public UserDTO(Integer id, @NotNull @NotBlank String name, @NotNull @NotBlank @Email String mail,
			@NotNull @NotBlank String password, @NotNull @NotBlank String phone) {
		super();
		this.id = id;
		this.name = name;
		this.mail = mail;
		this.password = password;
		this.phone = phone;
	}

}
