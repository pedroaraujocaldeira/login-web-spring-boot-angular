package br.com.zerotoproject.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import br.com.zerotoproject.dto.UserDTO;
import br.com.zerotoproject.model.AuthenticationRequest;
import br.com.zerotoproject.model.AuthenticationResponse;
import br.com.zerotoproject.service.UserAuthenticationService;
import br.com.zerotoproject.service.UserService;
import br.com.zerotoproject.util.JWTUtils;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
	
    private static final Gson gson = new Gson();
	
	@Autowired
	private UserService userService;
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserAuthenticationService userDetailsService;

	@Autowired
	JWTUtils JWTUtils;
	
	@PostMapping("/authenticate")
	public ResponseEntity<?> createAuthenticationToken(
			@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			return new ResponseEntity<>("BadCredentials",HttpStatus.UNAUTHORIZED);
		}
		UserDetails user = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		String jwt = JWTUtils.generateToken(user);
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
	
	@PostMapping
	public ResponseEntity<?> createUser(@Valid @RequestBody UserDTO user) {
		return ResponseEntity.ok(gson.toJson(userService.createUser(user)));
	}

}
