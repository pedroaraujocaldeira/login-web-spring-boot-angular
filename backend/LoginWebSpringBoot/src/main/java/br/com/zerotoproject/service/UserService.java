package br.com.zerotoproject.service;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.zerotoproject.dto.UserDTO;
import br.com.zerotoproject.exception.ResourceNotFoundException;
import br.com.zerotoproject.mail.Mailer;
import br.com.zerotoproject.mail.Message;
import br.com.zerotoproject.model.UserCredencial;
import br.com.zerotoproject.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	Environment env;

	@Autowired
	Mailer mail;

	private UserCredencial ToEntities(@Valid UserDTO user) {

		UserCredencial u = new UserCredencial();
		u.setActive(true);
		u.setName(user.getName());
		u.setPhone(user.getPhone());
		u.setPassword(user.getPassword());
		u.setRole("ROLE_ADMIN");
		u.setUserName(user.getMail());

		return u;
	}

	public UserDTO ToEntities(UserCredencial user) {
		UserDTO u = new UserDTO();
		u.setId(user.getId());
		u.setMail(user.getUserName());
		u.setName(user.getName());
		u.setPassword(user.getPassword());
		return u;
	}

	@Transactional
	public ResponseEntity<?> createUser(@Valid UserDTO user) {
		String body = "";
		Optional<UserCredencial> userList = userRepository.findByUserName(user.getMail());
		if (userList.isPresent()) {
			return ResponseEntity.badRequest().body("Email inválido!");
		} else {

			String password = user.getPassword();
			body = String.format("Bem vindo, %s!", user.getName());
			String encodedPassword = passwordEncoder.encode(password);
			user.setPassword(encodedPassword);
			UserCredencial dto = ToEntities(user);
			userRepository.saveAndFlush(dto);
		}
//		enviarEmail(user.getMail(), body);
		return ResponseEntity.status(HttpStatus.OK).body("Usuário Cadastrado Com Sucesso, Acesse seu email!");
	}

	@Transactional
	public ResponseEntity<?> updateUser(@Valid UserDTO user) {

		Optional<UserCredencial> userUpdate = userRepository.findById(user.getId());
		if (userUpdate.isPresent()) {
			return ResponseEntity.ok(ToEntities(userRepository.save(ToEntities(user))));
		} else {
			return ResponseEntity.ok(ToEntities(user));
		}
	}

	@Transactional
	public ResponseEntity<?> deleteUser(Integer userId) {
		Optional<UserCredencial> userDelete = userRepository.findById(userId);
		if (userDelete.isPresent()) {
			try {
				userRepository.delete(userDelete.get());
				return ResponseEntity.status(HttpStatus.OK).body("Usuário deletado com sucesso!");
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.OK).body("Ocorreu um erro!");
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body("Usuário não existe!");
	}

	public ResponseEntity<?> getUser(HttpServletRequest request) throws ResourceNotFoundException {

		String mail = request.getUserPrincipal().getName();
		UserCredencial user = userRepository.findByUserName(mail)
				.orElseThrow(() -> new ResourceNotFoundException("Usuário para este id não existe"));

		return ResponseEntity.ok(ToEntities(user));

	}

	@Transactional
	public void enviarEmail(String mailSend, String body) {
		mail.enviar(new Message(env.getProperty("mail.smtp.username"), Arrays.asList("<" + mailSend + ">"),
				"Usuário Criado Com Sucesso!", body));
	}

	@Transactional
	public Object getUser(Integer userId) throws ResourceNotFoundException {
		UserCredencial user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("Usuário para este id não existe"));

		return ResponseEntity.ok(ToEntities(user));
	}

}
