package br.com.zerotoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class LoginWebSpringBootAngularApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(LoginWebSpringBootAngularApplication.class, args);
	}

}
