package br.com.zerotoproject.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.zerotoproject.model.UserCredencial;

@Repository
public interface UserRepository extends JpaRepository<UserCredencial, Integer> {

	Optional<UserCredencial> findByUserName(String userName);

}
